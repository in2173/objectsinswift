class Music {
  let notes: [String]

  init(notes: [String]) {
    self.notes = notes
  }

  func prepared() -> String {
    return notes.joined(separator: " ")
  }
}

let x = ["C#", "A", "F", "G", "B#"]
let bac = Music(notes:x)
print("Music notes: \(bac.prepared())")

let y = ["D#", "B-", "C"]
let song = Music(notes:y)
print("Music notes: \(song.prepared())")
