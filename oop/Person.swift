// This code is from App Development with Swift by Apple Inc (2017)
class Person {
  let name: String

  init(name: String) {
    self.name = name
  }

  func sayHello() {
    print("Hello, there!")
  }
 }

let person = Person(name: "Jasmine")
print(person.name)
person.sayHello()
