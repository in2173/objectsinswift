class Square {
    var length: Int = 1

    init(length: Int) {
        self.length = length
    }
}

var firstSquare = Square(length: 3)
print(firstSquare.length)

var secondSquare = Square(length: 10)
print(secondSquare.length)

if firstSquare.length > secondSquare.length {
    print("FIRST square is bigger")
} else {
    print("SECOND square is bigger")
}
