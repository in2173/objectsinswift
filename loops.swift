var x = ["apple", "banana", "carrot", "donut"]
for y in x {
  print(y)
}


var z = [3, 5, 12]
for n in z {
  print(n)
}


var p = [[3, 1], [25, 3, 2], ["a", "b", "c", "d"]]
for abc in p {
  print(abc)
}

var mmm = [Any]()
mmm = [3, 25, "banana"]
print(mmm)

for x in mmm {
  print(x)
}
